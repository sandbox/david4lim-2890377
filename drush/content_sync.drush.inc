<?php

/**
 * @file
 * Include file for drush commands.
 */

use Drupal\Core\Site\Settings;

/**
 * Implements hook_drush_command().
 */
function content_sync_drush_command() {
  $items['content-sync-export'] = [
    'description' => dt('Exports a single entity'),
    'arguments' => [
      'entity_type' => dt('The entity type to export.'),
      'entity_id' => dt('The ID(s) of the entity(ies) to export. Use comma to separate each ID.'),
    ],
    'options' => [
      'format' => dt('The desired format do export.'),
      'gzip' => dt('Compress files into gzip'),
      'destination' => dt('The target path for the exported content.'),
      'context' => dt('The serializer context. It must to be a JSON object.'),
      'context-source' => dt('The serializer context. It must to be a JSON file.'),
      'skip-references' => dt('Set it to true if you do not wat exporting entity references.'),
    ],
    'aliases' => ['cse'],
    'required-arguments' => 2,
  ];
  $items['content-sync-import'] = [
    'description' => dt('Import content to the current application'),
    'arguments' => [],
    'options' => [
      'format' => dt('The format that will be used to import the content.'),
      'no-update' => dt('Only imports the new entities, and skips the existing entities.'),
      'source' => dt('The source path of the directory or file to import.'),
      'preview' => dt('Shows a table with the task to apply.'),
      'context' => dt('The serializer context. It must to be a JSON object.'),
      'context-source' => dt('The serializer context. It must to be a JSON file.'),
    ],
    'aliases' => ['csi'],
    'required-arguments' => 0,
  ];

  return $items;
}

/**
 * Exports a piece of content and all its referenced entities.
 *
 * @param string $entity_type
 *   The entity type ID.
 * @param mixed $entity_id
 *   The entity ID to export.
 */
function drush_content_sync_export($entity_type, $entity_id) {
  $directory = drush_get_option('destination', '');

  if (empty($directory)) {
    $directory = Settings::get('content_sync_directory', '');
  }

  $format = drush_get_option('format', '');
  if (empty($format)) {
    $format = Settings::get('content_sync_format', '');
  }

  drush_print('Exporting entities ' . $entity_type . ':' . $entity_id . ' to ' . $directory);

  /** @var \Drupal\content_sync\ContentSyncManager $manager */
  $manager = \Drupal::service('content_sync.manager');
  if ($format) {
    $manager->setFormat($format);
  }
  $context = [
    'content_sync_directory' => $directory,
  ];
  $context += _content_sync_drush_get_context();
  $manager->setContext($context);

  $extension = _content_sync_extension($format);
  $skip_references = drush_get_option('skip-references', FALSE);
  $batch = [
    'operations' => [],
    'title' => dt('Exporting Entities'),
    'progress_message' => dt('@current entity of @total were processed.'),
  ];
  if (strpos($entity_id, 'bundle:') === 0 || $entity_id === 'all') {
    if ($entity_id === 'all') {
      $entity_id = NULL;
    }
    else {
      $bundle = str_replace('bundle:', '', $entity_id);
    }
    $entity_ids = _content_sync_drush_get_entities($entity_type, $bundle);
    foreach ($entity_ids as $entity_id) {
      $batch['operations'][] = [
        '_content_sync_drush_batch_export_content',
        [
          $entity_type,
          $entity_id,
          $context,
          $skip_references,
          $directory,
          $format,
        ],
      ];
    }
    // Get the batch process all ready!
    batch_set($batch);
    // Start processing the batch operations.
    drush_backend_batch_process();
  }
  else {
    if ($entity_id === 'all') {
      $entity_id = NULL;
    }
    else {
      $entity_id = explode(',', $entity_id);
    }
    $entities_by_type = $manager->exportContent($entity_type, $entity_id, $context, $skip_references);
    _content_sync_drush_save_exported_entities($entities_by_type, $directory, $extension);
  }


  $gzip = drush_get_option('gzip', FALSE);
  if (!$gzip) {
    drush_print('Entities exported!');
  }
  else {
    $destination = drush_get_option('destination', FALSE);
    if (!$destination) {
      $config = \Drupal::config('system.site');
      $site_name = preg_replace("/[^a-zA-Z0-9]+/", "", $config->get('name'));
      $request_time = date('YmdHi');
      $destination = rtrim($directory, '/');
      $destination .= strtolower("/{$site_name}_{$request_time}.tar.gz");
    }

    $tmp_directory = rtrim(file_directory_temp(), '/') . '/content_sync_' . uniqid();
    file_prepare_directory($tmp_directory, FILE_CREATE_DIRECTORY);
    foreach ($entities_by_type as $uuid => $entity) {
      $entity_type_folder = "{$tmp_directory}/{$entity['entity_type_id']}";
      file_prepare_directory($entity_type_folder, FILE_CREATE_DIRECTORY);
      file_put_contents("{$entity_type_folder}/{$uuid}.{$extension}", $entity['entity']);
    }
    drush_shell_exec("tar -C %s -czf %s ./", $tmp_directory, $destination);
    file_unmanaged_delete_recursive($tmp_directory);
    drush_print('Entities compressed and exported to %s.', $destination);
  }
}

function _content_sync_drush_batch_export_content($entity_type, $entity_id, $cs_context, $skip_references, $directory, $format, &$context) {
  $manager = \Drupal::service('content_sync.manager');
  if ($format) {
    $manager->setFormat($format);
  }
  $extension = _content_sync_extension($format);
  $entities_by_type = $manager->exportContent($entity_type, [$entity_id], $cs_context, $skip_references);
  _content_sync_drush_save_exported_entities($entities_by_type, $directory, $extension);
  $context['message'] = drush_print("Exported entity {$entity_type}:{$entity_id}");
}

/**
 * Import the content placed in the import directory.
 *
 */
function drush_content_sync_import() {
  $source = drush_get_option('source', '');

  if (empty($source)) {
    $source = Settings::get('content_sync_directory', '');
  }

  $format = drush_get_option('format', '');
  if (empty($format)) {
    $format = Settings::get('content_sync_format', '');
  }

  /**
   * @var \Drupal\content_sync\ContentSyncManagerInterface $manager
   */
  $manager = \Drupal::service('content_sync.manager');
  if (!empty($format)) {
    $manager->setFormat($format);
  }
  if (drush_get_option('no-update', FALSE)) {
    $manager->setUpdateEntities(FALSE);
  }


  $delete_source = FALSE;

  if (is_file($source)) {
    $source_dir = dirname($source) . '/' . basename($source, '.tar.gz');
    file_prepare_directory($source_dir, FILE_CREATE_DIRECTORY);
    drush_shell_exec("tar -C %s -xvzf %s", $source_dir, $source);
    $delete_source = TRUE;
    $source = $source_dir;
  }

  $context = [
    'content_sync_directory' => $source,
  ];
  $context += _content_sync_drush_get_context();
  $manager->setContext($context);

  if (drush_get_option('preview', FALSE)) {
    $rows = [['Type', 'Bundle', 'Label', 'UUID', 'Operation']];
    $colors = [
      'create' => drush_get_context('DRUSH_NOCOLOR') ? "%s" : "\033[1;32;40m\033[1m%s\033[0m",
      'update' => drush_get_context('DRUSH_NOCOLOR') ? "%s" : "\033[1;33;40m\033[1m%s\033[0m",
    ];
    foreach ($manager->previewContent($source) as $type => $contents) {
      foreach ($contents as $content) {
        if ($content['is_new'] || !drush_get_option('no-update', FALSE)) {
          $rows[] = [
            $type,
            $content['bundle'],
            $content['label'],
            $content['uuid'],
            $content['is_new'] ? sprintf($colors['create'], 'create') : sprintf($colors['update'], 'update'),
          ];
        }
      }
    }
    if (count($rows) > 1) {
      drush_print('The following entities will be imported:');
      drush_print_table($rows, TRUE);
      if (drush_confirm('Are you sure you want to import the above entities?')) {
        $manager->importContent($source);
        drush_print('Entities imported!');
      }
      else {
        drush_user_abort();
      }
      if ($delete_source) {
        file_unmanaged_delete_recursive($source);
      }
    }
    else {
      drush_print('There are no entities to import');
    }
  }
  else {
    if (drush_confirm(sprintf('Are you sure you want to import the entities from %s?', $source))) {
      //      $manager->importContent($source);
      $batch = _content_sync_drush_generate_import_batch($manager, $source);
      if (!empty($batch)) {
        // Get the batch process all ready!
        batch_set($batch);
        // Start processing the batch operations.
        drush_backend_batch_process();
        drush_print('Entities imported!');
      }
      else {
        drush_print('There are no entities to import');
      }
    }
    else {
      drush_user_abort();
    }
    if ($delete_source) {
      file_unmanaged_delete_recursive($source);
    }
  }
}


function _content_sync_drush_generate_import_batch(\Drupal\content_sync\ContentSyncManager $manager, $directory) {
  $queue = $manager->generateImportQueue($directory);
  $batch = FALSE;
  if (!empty($queue)) {
    $batch = [
      'operations' => [],
      'title' => dt('Exporting Entities'),
      'progress_message' => dt('@current entity of @total were processed.'),
    ];
    foreach ($queue as $item) {
      $decoded_entity = $item['decoded_entity'];
      $entity_type_id = $item['entity_type_id'];
      $batch['operations'][] = [
        '_content_sync_drush_batch_import_content',
        [
          $decoded_entity,
          $entity_type_id,
          $manager->getContext(),
          $directory,
          $manager->getFormat(),
        ],
      ];
    }
  }
  return $batch;
}

function _content_sync_drush_batch_import_content($decoded_entity, $entity_type_id, $cs_context, $directory, $format, &$context) {
  /** @var \Drupal\content_sync\ContentSyncManager $manager */
  $manager = \Drupal::service('content_sync.manager');
  if (!empty($format)) {
    $manager->setFormat($format);
  }
  if (drush_get_option('no-update', FALSE)) {
    $manager->setUpdateEntities(FALSE);
  }
  $manager->setContext($cs_context);
  $entity = $manager->importSingleContent($decoded_entity, $entity_type_id, $directory);
  if (!empty($entity)) {
    $context['message'] = dt('Imported content @label (@entity_type: @id).', [
      '@label' => $entity->label(),
      '@id' => $entity->id(),
      '@entity_type' => $entity->getEntityTypeId(),
    ]);
    $context['results'][] = TRUE;
  }
  else {
    $context['message'] = dt('Error exporting content of type @entity_type.', [
      '@entity_type' => $entity_type_id,
    ]);
  }
}

function _content_sync_drush_get_context() {
  $context = [];
  $context_file = drush_get_option('context-source', FALSE);
  if (file_exists($context_file)) {
    $context_file_content = file_get_contents($context_file);
    $context = json_decode($context_file_content, TRUE);
    if (!is_array($context)) {
      $context = [];
    }
  }
  $extra_context = drush_get_option('context', FALSE);
  if ($extra_context) {
    $extra_context = json_decode($extra_context, TRUE);
    if (is_array($extra_context)) {
      $context += $extra_context;
    }
  }
  return $context;
}

function _content_sync_drush_get_entities($entity_type, $bundle = NULL) {
  /** @var \Drupal\Core\Entity\Query\QueryInterface $entity_ids */
  $query = \Drupal::entityQuery($entity_type);
  if (!empty($bundle)) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $bundle_key = $entity_type_manager->getDefinition($entity_type)
                                      ->getKey('bundle');
    $query->condition($bundle_key, $bundle, '=');
  }
  $entity_ids = $query->execute();
  return $entity_ids;
}

function _content_sync_drush_save_exported_entities(array $entities_by_type, $directory, $extension) {
  static $uuids = [];
  foreach ($entities_by_type as $uuid => $entity) {
    if (in_array($uuid, $uuids)) {
      continue;
    }
    $entity_type_folder = "{$directory}/{$entity['entity_type_id']}";
    file_prepare_directory($entity_type_folder, FILE_CREATE_DIRECTORY);
    file_put_contents("{$entity_type_folder}/{$uuid}.{$extension}", $entity['entity']);
    drush_print("Created file: {$entity_type_folder}/{$uuid}.{$extension}");
    $uuids[] = $uuid;
  }
}