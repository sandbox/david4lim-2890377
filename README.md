# CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Setup
- Usage

  - Export
  - Import

- To do

# INTRODUCTION

This module brings solution for import content in Drupal 8.

# REQUIREMENTS

**Core**

- Serialization

# INSTALLATION

Content Synchronizer can be installed via the standard Drupal installation
process.

# SETUP

You can define the basic setup in your settings.php:

```
# The default path where the content will be exported to/imported from.
$settings['content_sync_directory'] = 'content_sync/path';
# The default format used to export/import content.
$settings['content_sync_format'] = 'yaml';
```

# USAGE

This module provides drush commands to export and to import content.

## Export

For export you can use the command `content-sync-export` or the alias `cse`:

```
drush content-sync-export {entity_type} {ID(s)}
drush cse {entity_type} {ID(s)}
```

For example, if you want to export a single node you can use:

```
drush cse node 1
```

For multiple contents (with the same entity_type_id) you can separate the nids
using commas:

```
drush cse node 1,2,4
```

### Supported Entity Types:

Anything that extends `\Drupal\Core\Entity\ContentEntityInterface`

## Import

For import content you can use the command `content-sync-import` or the alias
`csi`:

```
drush content-sync-import
drush csi
```

**Flags:** For importing only new content, and omit the existing content:
`drush csi --no-update`

**Important:** When you are exporting files or images, you must put the files in
the subfolder `files` inside the default import path. Also you must to use the
same folder structure of the you original file URI, and to specify the correct
`StreamWrapper`.

For example, if the original URI is `public://2016-01/file.txt`, it's necessary
to copy the file `file.txt` into
`{content_sync_directory}/files/public/2016-01/file.txt`

# TO DO

**High priority**

- ~~When you're exporting file entities, copy the files into the export dir.~~
- ~~Add support to import user entities.~~
- ~~Add new param to `csi` command to skip to update existing contents.~~

**Medium priority**

- Implement bulk operations.
- Write test cases.

**Low priority**

- Add support to hal_json.
- Add "delete all uuids from folder", "clean all before importing" to drush csi.
