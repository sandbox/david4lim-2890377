<?php

namespace Drupal\content_sync\Normalizer;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorManager;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorTrait;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\serialization\Normalizer\ContentEntityNormalizer as BaseContentEntityNormalizer;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Adds the file URI to embedded file entities.
 */
class ContentEntityNormalizer extends BaseContentEntityNormalizer {

  use SyncNormalizerDecoratorTrait;

  /**
   * @var SyncNormalizerDecoratorManager
   */
  protected $decoratorManager;

  /**
   * Constructs an EntityNormalizer object.
   *
   * @param EntityTypeManagerInterface $entity_manager
   *
   * @param SyncNormalizerDecoratorManager $decorator_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, SyncNormalizerDecoratorManager $decorator_manager) {
    parent::__construct($entity_manager);
    $this->decoratorManager = $decorator_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    if (is_null($data)) {
      return NULL;
    }
    $original_data = $data;

    // Get the entity type ID while letting context override the $class param.
    $entity_type_id = !empty($context['entity_type']) ? $context['entity_type'] : $this->entityManager->getEntityTypeFromClass($class);

    // Resolve references
    $this->fixReferences($data, $entity_type_id);

    // Remove invalid fields
    $this->cleanupData($data, $entity_type_id);

    $entity = parent::denormalize($data, $class, $format, $context);

    $this->decorateDenormalizedEntity($entity, $original_data, $format, $context);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized_data = parent::normalize($object, $format, $context);

    $normalized_data['_content_sync'] = [];
    /**
     * @var \Drupal\Core\Entity\ContentEntityBase $object
     */
    $referenced_entities = $object->referencedEntities();
    if (!empty($referenced_entities)) {
      $normalized_data['_content_sync']['referenced_entities'] = [];
      $dependencies = [];
      foreach ($referenced_entities as $entity) {
        if (is_a($entity, ContentEntityBase::class)) {
          $dependencies[$entity->getEntityTypeId()][$entity->uuid()] = $entity->uuid();
        }
      }
      $normalized_data['_content_sync']['referenced_entities'] = $dependencies;
    }

    // Decorate normalized entity before retuning it.
    if (is_a($object, ContentEntityInterface::class, TRUE)) {
      $this->decorateNormalization($normalized_data, $object, $format, $context);
    }
    return $normalized_data;
  }

  /**
   * @inheritdoc
   */
  public function supportsNormalization($data, $format = NULL) {
    return parent::supportsNormalization($data, $format);
  }

  /**
   * @inheritdoc
   */
  public function supportsDenormalization($data, $type, $format = NULL) {
    return parent::supportsDenormalization($data, $type, $format);
  }


  /**
   * @inheritdoc
   */
  protected function getDecoratorManager() {
    return $this->decoratorManager;
  }

  /**
   * @param array $data
   * @param $entity_type_id
   *
   * @return array
   */
  protected function fixReferences(&$data, $entity_type_id) {
    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type_definition */
    // Get the entity type definition.
    $entity_type_definition = $this->entityManager->getDefinition($entity_type_id, FALSE);
    if ($entity_type_definition->hasKey('bundle')) {

      $bundle_key = $entity_type_definition->getKey('bundle');
      $bundle = $data[$bundle_key];
      if (is_array($bundle) && isset($bundle[0]['target_id'])) {
        $bundle = $data[$bundle_key][0]['target_id'];
      }
      $field_definitions = $this->entityManager->getFieldDefinitions($entity_type_id, $bundle);
      foreach ($field_definitions as $field_name => $field_definition) {
        // We are only interested in importing content entities.
        if (!is_a($field_definition->getClass(), '\Drupal\Core\Field\EntityReferenceFieldItemList', TRUE)) {
          continue;
        }
        if (is_array($data[$field_name]) && !empty($data[$field_name])) {
          $key = $field_definition->getFieldStorageDefinition()
                                  ->getMainPropertyName();
          foreach ($data[$field_name] as &$item) {
            if (!empty($item[$key]) && !empty($item['target_uuid'])) {
              $reference = $this->entityManager->loadEntityByUuid($item['target_type'], $item['target_uuid']);
              if ($reference) {
                $item[$key] = $reference->id();
                if (is_a($reference, RevisionableInterface::class, TRUE)) {
                  $item['target_revision_id'] = $reference->getRevisionId();
                }
              }
            }
          }
        }
      }
    }
    return $data;
  }

  /**
   * @param $data
   * @param $entity_type_id
   */
  protected function cleanupData(&$data, $entity_type_id) {
    $field_names = array_keys($this->entityManager->getBaseFieldDefinitions($entity_type_id));
    foreach ($data as $field_name => $field_data) {
      if (!in_array($field_name, $field_names)) {
        unset($data[$field_name]);
      }
    }
  }

}
