<?php

namespace Drupal\content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\Component\Serialization\Yaml;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a decorator to map field names.
 *
 * @SyncNormalizerDecorator(
 *   id = "bundle_mapper",
 *   name = @Translation("Bundle Mapper"),
 * )
 */
class BundleMapper extends SyncNormalizerDecoratorBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
    $bundle_map = $this->getBundleMap($context);
    if (!empty($bundle_map)) {
      foreach ($bundle_map as $entity_type_id => $mappings) {
        if ($entity->getEntityTypeId() == $entity_type_id) {
          $bundle_key = $entity->getEntityType()->getKey('bundle');
          if ($bundle_key) {
            foreach ($mappings as $mapping) {
              if (!empty($mapping['remote_bundle']) && !empty($mapping['local_bundle']) && $entity->bundle() == $mapping['local_bundle']) {
                $normalized_entity[$bundle_key][0]['target_id'] = $mapping['remote_bundle'];
                break 2;
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    $bundle_map = $this->getBundleMap($context);
    if (!empty($bundle_map)) {
      foreach ($bundle_map as $entity_type_id => $mappings) {
        $entity_type = $this->entityTypeManager->getStorage($type)->getEntityType();
        if ($type == $entity_type_id) {
          $bundle_key = $entity_type->getKey('bundle');
          if ($bundle_key) {
            foreach ($mappings as $mapping) {
              if (!empty($mapping['remote_bundle']) && !empty($mapping['local_bundle']) && $normalized_entity[$bundle_key][0]['target_id'] == $mapping['remote_bundle']) {
                $normalized_entity[$bundle_key][0]['target_id'] = $mapping['local_bundle'];
                break 2;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Extract the field map from the serializer context.
   *
   * @param array $context
   *   The serializer context.
   *
   * @return array
   *   The field map.
   */
  protected function getBundleMap($context) {
    $bundle_map = [];
    if (!empty($context['bundle_map'])) {
      $bundle_map = $context['bundle_map'];
    }
    elseif (!empty($context['content_sync_directory'])) {
      $file = realpath($context['content_sync_directory'] . '/bundle_map.yml');
      if (file_exists($file)) {
        $bundle_map = Yaml::decode(file_get_contents($file));
        if (!empty($bundle_map)) {
          return $bundle_map;
        }
      }
    }
    return $bundle_map;
  }

}
