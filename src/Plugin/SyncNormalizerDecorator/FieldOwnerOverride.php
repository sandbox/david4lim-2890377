<?php

namespace Drupal\content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;

/**
 * Provides a decorator to set default vaules to the fields.
 *
 * @SyncNormalizerDecorator(
 *   id = "field_owner_override",
 *   name = @Translation("Field Override Value"),
 * )
 */
class FieldOwnerOverride extends SyncNormalizerDecoratorBase {


  /**
   * {@inheritdoc}
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    if(!empty($context['user-ownership'])) {
      $normalized_entity['uid'] = $context['user-ownership'];
    }
  }


}
