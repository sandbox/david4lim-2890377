<?php

namespace Drupal\content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\Component\Serialization\Yaml;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides a decorator to set default vaules to the fields.
 *
 * @SyncNormalizerDecorator(
 *   id = "field_remover",
 *   name = @Translation("Field Remover"),
 * )
 */
class FieldRemover extends SyncNormalizerDecoratorBase {

  /**
   * {@inheritdoc}
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
    $entity_type_id = $entity->getEntityTypeId();
    $fields_to_remove = $this->getFieldsToRemove($context, $entity_type_id);
    if (!empty($fields_to_remove)) {
      foreach ($fields_to_remove as $field) {
        if (isset($normalized_entity[$field])) {
          unset($normalized_entity[$field]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    $fields_to_remove = $this->getFieldsToRemove($context, $type);
    if (!empty($fields_to_remove)) {
      foreach ($fields_to_remove as $field) {
        if (isset($normalized_entity[$field])) {
          unset($normalized_entity[$field]);
        }
      }
    }
  }

  /**
   * Extract the list of fields to remove for the given context and entity type.
   *
   * @param array $context
   *   The serializer context.
   *
   * @param array $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   The field map.
   */
  protected function getFieldsToRemove($context, $entity_type_id) {
    $fields_to_remove= [];
    if (!empty($context['fields_to_remove'][$entity_type_id])) {
      $fields_to_remove= $context['fields_to_remove'][$entity_type_id];
    }
    elseif (!empty($context['content_sync_directory'])) {
      $file = realpath($context['content_sync_directory'] . '/fields_to_remove.yml');
      if ($file && file_exists($file)) {
        $fields_to_remove_yml= Yaml::decode(file_get_contents($file));
        if (!empty($fields_to_remove_yml[$entity_type_id])) {
          $fields_to_remove = $fields_to_remove_yml[$entity_type_id];
        }
      }
    }
    return $fields_to_remove;
  }

}
