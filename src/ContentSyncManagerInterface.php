<?php

namespace Drupal\content_sync;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface ContentSyncManagerInterface.
 *
 * @package Drupal\content_sync
 */
interface ContentSyncManagerInterface {

  /**
   * Imports default content for a given folder.
   *
   * @param string $folder
   *   The folder to import the content.
   *
   * @return array[\Drupal\Core\Entity\EntityInterface]
   *   The created entities.
   */
  public function importContent($folder);

  /**
   * Preview the content to import for a given folder.
   *
   * @param string $folder
   *   The folder to import the content.
   *
   * @return array[]
   *   The information about the entities to be imported.
   */
  public function previewContent($folder);

  /**
   * Exports a single entity as importContent expects it.
   *
   * @param string $entity_type
   *   The entity type ID.
   * @param array $entity_ids
   *   The entity IDs to export.
   * @param array $context
   *   The serializer context.
   *
   * @return string
   *   The rendered export as hal.
   */
  public function exportContent($entity_type, $entity_ids, $context = []);

  /**
   * Exports a list of entities as importContent expects it.
   *
   * @param EntityInterface[] $entities
   *   The entities.
   * @param array $context
   *   The Serializer context.
   *
   * @return string
   *   The rendered export as hal.
   */
  public function exportEntities($entities, $context = []);

  /**
   * Exports a list of entities for a given bundle as importContent expects it.
   *
   * @param string $entity_type
   *   The entity type ID.
   * @param string $bundle
   *   The entity IDs to export.
   * @param array $context
   *   The serializer context.
   *
   * @return string
   *   The rendered export as hal.
   */
  public function exportContentByBundle($entity_type, $bundle, $context = []);

  /**
   * Prepares the entity.
   *
   * @param ContentEntityInterface $entity
   *   The content entity.
   *
   * @return ContentEntityInterface
   *   The prepared content entity.
   */
  public function prepareEntity(ContentEntityInterface $entity);

  /**
   * Validates the entity.
   *
   * @param ContentEntityInterface $entity
   *   The content entity to be validated.
   *
   * @return bool
   *   True if content entity is valid.
   */
  public function validateEntity(ContentEntityInterface $entity);

  public function getEntityReferences(ContentEntityInterface $entity);

  public function exportEntity(ContentEntityInterface $entity, array $context = []);
}
