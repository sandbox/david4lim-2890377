<?php

namespace Drupal\content_sync\DependencyResolver;


use Drupal\Component\Graph\Graph;

class ImportQueueResolver implements ContentSyncResolverInterface {

  public function resolve(array $normalized_entities) {
    $queue = [];
    $graph = [];
    $uuids = [];
    foreach ($normalized_entities as $entity_type_id => $entities) {
      $uuid_key = \Drupal::service('entity_type.manager')
                         ->getDefinition($entity_type_id)
                         ->getKey('uuid');
      foreach ($entities as $entity) {
        $uuid = $entity[$uuid_key][0]['value'];
        if (!empty($entity['_content_sync']['referenced_entities'])) {
          foreach ($entity['_content_sync']['referenced_entities'] as $ref_entity_type_id => $references) {
            foreach ($references as $reference) {
              if (!empty($normalized_entities[$entity_type_id][$uuid])) {
                $graph["$entity_type_id:$uuid"]['edges']["$ref_entity_type_id:$reference"] = 1;
              }
            }
          }
        }
        else {
          $uuids[] = "$entity_type_id:$uuid";
          $queue[] = [
            'entity_type_id' => $entity_type_id,
            'decoded_entity' => $entity,
          ];
        }
      }
    }
    $graph = new Graph($graph);
    $entities = $graph->searchAndSort();
    uasort($entities, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    foreach ($entities as $uuid => $vertex) {
      foreach ($vertex['edges'] as $key => $value) {
        if (!in_array($key, $uuids) && $uuid != $key) {
          $uuids[] = $key;
          $ids = explode(':', $key);
          $queue[] = [
            'entity_type_id' => $ids[0],
            'decoded_entity' => $normalized_entities[$ids[0]][$ids[1]],
          ];
        }
      }
      $uuids[] = $uuid;
      $ids = explode(':', $uuid);
      $queue[] = [
        'entity_type_id' => $ids[0],
        'decoded_entity' => $normalized_entities[$ids[0]][$ids[1]],
      ];
    }
    return $queue;
  }

}