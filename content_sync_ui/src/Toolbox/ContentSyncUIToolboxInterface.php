<?php

namespace Drupal\content_sync_ui\Toolbox;

use Drupal\Core\Entity\ContentEntityInterface;

interface ContentSyncUIToolboxInterface {

  /**
   * @param $serialized_entity
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param $destination
   * @param string $file_extension
   *
   * @return mixed
   */
  public function saveExportedEntity($serialized_entity, ContentEntityInterface $entity, $destination, $file_extension = 'json');

  /**
   * @param $directory
   * @param bool $delete_source
   *
   * @return string|bool
   */
  public function compress($directory, $delete_source = FALSE);
}