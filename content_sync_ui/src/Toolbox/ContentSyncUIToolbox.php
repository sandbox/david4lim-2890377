<?php

namespace Drupal\content_sync_ui\Toolbox;


use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Entity\ContentEntityInterface;

class ContentSyncUIToolbox implements ContentSyncUIToolboxInterface {

  const EXPORTED_CONTENT_URI = 'public://content_exported.tar.gz';

  public function saveExportedEntity($serialized_entity, ContentEntityInterface $entity, $destination, $file_extension = 'json') {
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
    $entity_type_folder = "{$destination}/{$entity->getEntityTypeId()}";
    file_prepare_directory($entity_type_folder, FILE_CREATE_DIRECTORY);
    file_put_contents("{$entity_type_folder}/{$entity->uuid()}.{$file_extension}", $serialized_entity);
  }

  public function compress($directory, $delete_source = FALSE) {
    $url = FALSE;
    $file = \Drupal::service('file_system')
                  ->realpath(self::EXPORTED_CONTENT_URI);
    file_unmanaged_delete($file);
    $archiver = new ArchiveTar($file, 'gz');
    if ($archiver->addModify($directory, '', $directory)) {
      $url = file_create_url(self::EXPORTED_CONTENT_URI);
    }
    if ($delete_source) {
      file_unmanaged_delete($directory);
    }
    return $url;
  }

}