<?php

namespace Drupal\content_sync_ui\Form;

use Drupal\content_sync\ContentSyncManagerInterface;
use Drupal\content_sync_ui\Toolbox\ContentSyncUIToolboxInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ContentExportMultiple
 *
 * @package Drupal\content_sync_ui\Form
 */
class ContentExportMultiple extends ConfirmFormBase {

  /**
   * Entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Private Temp Store Factory service.
   *
   * @var PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\content_sync\ContentSyncManagerInterface
   */
  protected $contentSyncManager;

  /**
   * @var \Drupal\content_sync_ui\Toolbox\ContentSyncUIToolboxInterface
   */
  protected $contentSyncUIToolbox;

  /**
   * @var array
   */
  protected $entityList = [];

  protected $formats;

  /**
   * Constructs a ContentSyncMultiple form object.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity type manager.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $manager, ContentSyncManagerInterface $content_sync_manager, ContentSyncUIToolboxInterface $content_sync_ui_toolbox, array $formats) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypeManager = $manager;
    $this->contentSyncManager = $content_sync_manager;
    $this->contentSyncUIToolbox = $content_sync_ui_toolbox;
    $this->formats = $formats;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('entity_type.manager'),
      $container->get('content_sync.manager'),
      $container->get('content_sync_ui.toolbox'),
      $container->getParameter('serializer.formats')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_sync_export_multiple_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->entityList), 'Are you sure you want to export this item?', 'Are you sure you want to export these items?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Export');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->entityList = $this->tempStoreFactory->get('content_sync_ui_multiple_confirm')
      ->get($this->currentUser()
        ->id());

    if (empty($this->entityList)) {
      return new RedirectResponse($this->getCancelUrl()
        ->setAbsolute()
        ->toString());
    }

    // List of items to synchronize.
    $items = [];
    foreach ($this->entityList as $uuid => $entity_info) {
      $storage = $this->entityTypeManager->getStorage($entity_info['entity_type']);
      $entity = $storage->load($entity_info['entity_id']);
      if (!empty($entity)) {
        $items[$uuid] = $entity->label();
      }
    }
    $form['content_list'] = [
      '#theme' => 'item_list',
      '#title' => 'Content List.',
      '#items' => $items,
    ];
    $form = parent::buildForm($form, $form_state);

    $options = array_combine($this->formats, $this->formats);
    $form['format'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => 'Format',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('confirm') && !empty($this->entityList)) {
      $format = $form_state->getValue('format');
      $tmp = file_directory_temp() . '/' . uniqid('content_sync_');
      $context = [
        'content_sync_directory' => $tmp,
      ];
      $batch = [
        'title' => $this->t('Exporting Content...'),
        'message' => $this->t('Exporting Content...'),
        'operations' => [],
        'finished' => [$this, 'batchFinished'],
      ];

      foreach ($this->entityList as $entity_info) {
        $entity = $this->entityTypeManager
          ->getStorage($entity_info['entity_type'])
          ->load($entity_info['entity_id']);
        $batch['operations'][] = [
          [$this, 'processItem'],
          [$entity, $format, $context],
        ];
      }
      $batch['operations'][] = [
        [$this, 'generateCompressedFile'],
        [$tmp],
      ];
      batch_set($batch);
    }
    else {
      $form_state->setRedirect('system.admin_content');
    }
  }

  /**
   * Batch operations callback.
   */
  public function generateCompressedFile($directory, &$context) {
    $context['message'] = $directory;
    $file = $this->contentSyncUIToolbox->compress($directory, TRUE);
    if ($file) {
      drupal_set_message($file);
    }
    else {
      drupal_set_message('Error compressing content.', 'error');
    }
  }
  /**
   * Batch operations callback.
   */
  public function processItem(ContentEntityInterface $entity, $format, $content_sync_context = [], &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['entity_references'] = $this->contentSyncManager->getEntityReferences($entity);
      $context['sandbox']['max'] = count($context['sandbox']['entity_references']) + 1;
    }

    $error = FALSE;
    if (!empty($context['sandbox']['entity_references'])) {
      $referenced_entity = array_pop($context['sandbox']['entity_references']);
      $sync = $referenced_entity;
      $exported_entity = $this->contentSyncManager->exportEntity($referenced_entity, $content_sync_context);
      $this->contentSyncUIToolbox->saveExportedEntity($exported_entity['entity'], $exported_entity['original_entity'], $content_sync_context['content_sync_directory']);
    }
    else {
      $context['results'][] = TRUE;
      $sync = $entity;
      $exported_entity = $this->contentSyncManager->exportEntity($entity, $content_sync_context);
      $this->contentSyncUIToolbox->saveExportedEntity($exported_entity['entity'], $exported_entity['original_entity'], $content_sync_context['content_sync_directory']);
    }

    $context['sandbox']['progress']++;
    $context['message'] = $this->t('Exported content @label (@entity_type: @id).', [
      '@label' => $sync->label(),
      '@id' => $sync->id(),
      '@entity_type' => $sync->getEntityTypeId(),
    ]);
    if ($error) {
      $context['message'] = $this->t('Error exporting content @label (@entity_type: @id).', [
        '@label' => $sync->label(),
        '@id' => $sync->id(),
        '@entity_type' => $sync->getEntityTypeId(),
      ]);
    }
    if ($error) {
      drupal_set_message($context['message'], 'error');
    }
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Callback for batch finished.
   */
  public function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One content exported.', '@count contents exported.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
    $this->tempStoreFactory->get('content_sync_ui_multiple_confirm')
      ->delete(\Drupal::currentUser()->id());
  }

}
