<?php

namespace Drupal\content_sync_ui\Form;

use Drupal\content_sync\ContentSyncManagerInterface;
use Drupal\content_sync_ui\Toolbox\ContentSyncUIToolboxInterface;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContentSyncImport
 *
 * @package Drupal\content_sync_ui\Form
 */
class ContentSyncImport extends FormBase {

  /**
   * Entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Private Temp Store Factory service.
   *
   * @var PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\content_sync\ContentSyncManagerInterface
   */
  protected $contentSyncManager;

  /**
   * @var \Drupal\content_sync_ui\Toolbox\ContentSyncUIToolboxInterface
   */
  protected $contentSyncUIToolbox;

  /**
   * @var array
   */
  protected $entityList = [];

  protected $formats;

  /**
   * Constructs a ContentSyncMultiple form object.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $manager, ContentSyncManagerInterface $content_sync_manager, ContentSyncUIToolboxInterface $content_sync_ui_toolbox, array $formats) {
    $this->entityTypeManager = $manager;
    $this->contentSyncManager = $content_sync_manager;
    $this->contentSyncUIToolbox = $content_sync_ui_toolbox;
    $this->formats = $formats;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('content_sync.manager'),
      $container->get('content_sync_ui.toolbox'),
      $container->getParameter('serializer.formats')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_sync_import';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->entityList), 'Are you sure you want to export this item?', 'Are you sure you want to export these items?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Export');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($folder = $form_state->getValue('import_folder')) {
      $form['import_folder'] = [
        '#type' => 'hidden',
        '#value' => $folder,
      ];
      $entity_types = $this->contentSyncManager->getEntityTypes($folder);
      $form['entity_types'] = [
        '#type' => 'checkboxes',
        '#options' => $entity_types,
        '#default_value' => array_keys($entity_types),
        '#title' => $this->t('Entity Types to import'),
      ];

      $options = array_combine($this->formats, $this->formats);
      $form['format'] = [
        '#type' => 'select',
        '#options' => $options,
        '#title' => $this->t('Format'),
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import'),
      ];
    }
    else {
      $folder = Settings::get('content_sync_directory', FALSE);
      if ($folder) {
        $form['use_default'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Use default Content Sync folder'),
          '#description' => $this->t('Import content from @folder', ['@folder' => $folder]),
        ];
        $form['default_folder'] = [
          '#type' => 'hidden',
          '#value' => $folder,
        ];
      }

      $form['import_tar'] = [
        '#type' => 'file',
        '#title' => $this->t('Contents archive'),
        '#description' => $this->t('Allowed types: @extensions.', ['@extensions' => 'tar.gz tgz tar.bz2']),
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Upload'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $all_files = $this->getRequest()->files->get('files', []);
    if ($form_state->getValue('use_default') && !empty($all_files['import_tar'])) {
      $file_upload = $all_files['import_tar'];
      if ($file_upload->isValid()) {
        $form_state->setValue('import_tar', $file_upload->getRealPath());
        return;
      }
      $form_state->setErrorByName('import_tar', $this->t('The file could not be uploaded.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($tmp = $form_state->getValue('import_folder')) {
      $format = $form_state->getValue('format');
      $this->contentSyncManager->setFormat($format);
      $entity_types = array_filter($form_state->getValue('entity_types'));
      $batch = [
        'title' => $this->t('Importing Content...'),
        'message' => $this->t('Importing Content...'),
        'operations' => [
          [
            [$this, 'importContent'],
            [$tmp, $entity_types],
          ],
          [
            [$this, 'removeTemporalFolder'],
            [$tmp],
          ],
        ],
        'finished' => [$this, 'batchFinished'],
      ];

      batch_set($batch);
    }
    elseif ($file = $form_state->getValue('import_tar')) {
      $tmp = file_directory_temp() . '/' . uniqid('content_sync_');
      try {
        $archiver = new ArchiveTar($file, 'gz');
        $archiver->extract($tmp);
        $form_state->setValue('import_folder', $tmp);
        $form_state->setRebuild();
      } catch (\Exception $e) {
        drupal_set_message($this->t('Could not extract the contents of the tar file. The error message is <em>@message</em>', ['@message' => $e->getMessage()]), 'error');
      }
    }
    elseif ($folder = $form_state->getValue('default_folder')) {
      $form_state->setValue('import_folder', $folder);
      $form_state->setRebuild();
    }
    else {
      //      $form_state->setRedirect('system.admin_content');
    }
  }

  /**
   * Batch operations callback.
   */
  public function removeTemporalFolder($directory, &$context) {
    \Drupal::service('file_system')->unlink($directory, NULL);
    $context['message'] = 'Removed Import Queue';
  }

  /**
   * Batch operations callback.
   */
  public function importContent($directory, $entity_types, &$context) {
    if (empty($context['sandbox'])) {
      $queue = $this->contentSyncManager->generateImportQueue($directory);
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['queue'] = $queue;
      $context['sandbox']['max'] = count($queue);
    }
    if (!empty($context['sandbox']['queue'])) {
      $error = FALSE;
      $item = array_pop($context['sandbox']['queue']);
      $decoded_entity = $item['decoded_entity'];
      $entity_type_id = $item['entity_type_id'];
      if (in_array($entity_type_id, $entity_types)) {
        $entity = $this->contentSyncManager->importSingleContent($decoded_entity, $entity_type_id, $directory);
        $context['results'][] = TRUE;
        $context['sandbox']['progress']++;
        $context['message'] = $this->t('Imported content @label (@entity_type: @id).', [
          '@label' => $entity->label(),
          '@id' => $entity->id(),
          '@entity_type' => $entity->getEntityTypeId(),
        ]);
        unset($entity);
      }
      else {
        $context['results'][] = TRUE;
        $context['sandbox']['progress']++;
        $context['message'] = $this->t('Ignored entity of type @entity_type.', [
          '@entity_type' => $entity_type_id,
        ]);
      }
      if ($error) {
        $context['message'] = $this->t('Error exporting content of type @entity_type.', [
          '@entity_type' => $entity_type_id,
        ]);
      }
      if ($error) {
        drupal_set_message($context['message'], 'error');
      }
    }

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Callback for batch finished.
   */
  public function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One content exported.', '@count contents imported.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

}
